#===Task 1===
"""
განსაზღვრეთ სია, რომელშიდაც ჩაწერეთ მხოლოდ ის რიცხვები 10-დან 3000-მდე შუალედიდან,
რომლებსაც აქვთ მხოლოდ ორი გამყოფი, რომლიდანაც ერთი 7-ის ტოლია.
(დავუშვათ 1-და თავის თავი გამყოფი არ არის.  7-ის გარდა არსებობს მხოლოდ მეორე გამყოფიც,
მაგალითად 77 = 7*11 ჩაიწერება მასივში, 70 = 7*10=5*14 არ ჩაიწერება მასივში).
"""
# import math
# result = []
# N = 3001
# for n in range(14, N, 7):
#     print(n)
#     counter = 0
#     for i in range(2, int(math.sqrt(n))):
#         if n%i==0:
#             counter += 2
#     if counter==2:
#         result.append(n)
# print(result)
#===Task 2===
"""
მოცემულია ფორმულა y=3x-2, მოცემულია x-ის მნიშვნელობები, რომლებიც წარმოადგენენ ათწილად რიცხვებს, 
რიცხვები დაშორებულია ერთმანეთისგან „-“ ტირეებით 3,4–9-8.8-2 და ა.შ. 
შესაძლებელია მწკირვში იყოს მაქსმიმუმ 10 რიცხვი, შესაბამისი სტრიქონი წინასწარ დააგენერირეთ შემთხვევითად, 
დაწერეთ პროგრამა, რომელიც გამოითვლის შესაბამისი y-ის მნიშვნელობებს ზემოთ მოცემული ფორმულის 
მიხედვით და შესაბამის x-ის და y-ის მნიშვნელობებს ჩაწერს ფაილში.
"""
# import random
# def random_numbers():
#     r = random.randint(1, 10)
#     numbers = ""
#     for _ in range(r):
#         random_r = round(random.uniform(0, 100), 1)
#         print(random_r)
#         numbers += "-" + str(random_r)
#     return numbers[1:]
#
# def y(x):
#     return 3*x-2
#
# numbers = [float(i) for i in random_numbers().split("-")]
#
# result = ""
# for x in numbers:
#     y_val = round(y(x), 1)
#     result += f"{x} - {y_val}"+"\n"
#
# f = open("mid/task2.txt", "w")
# f.write(result)
# f.close()

#===Task _ 4 ====
"""
ჩაწერეთ data.xlsx ექსელის ფაილის „sheetOne“ ფურცელზე შემთხვევით 100 მონაცემი (100 სტრიქონი) 
ქვემოთ განსაზღვრული წესების შესაბამისად: 
	პირველ სვეტში (ID) ჩაწერეთ განსხვავებული რიცხვები [1000, 9999] შუალედიდან, 
დატოვეთ შემთხვევითი 5 უჯრა ცარიელი;
	მეორე სვეტში (Date) ჩაწერეთ შემთხვევით თარიღები 01-01-2019 - დან 31-01-2019, 
შემთხვევით 7 უჯრაში ჩაწერეთ NaN;
	მესამე სვეტში(Time) ჩაწერეთ შემთხვევითი რიცხვები [1, 8] შუალედიდან, 
დატოვეთ უჯრების 30% ცარიელი;
	მეოთხე სვეტში(Hourly_Wage) ჩაწერეთ შემთხვევითი განსხვავებული რიცხვები [10, 100] შუალედიდან, 
უჯრების 25%-ში ჩაწერეთ NaN.

"""
# import pandas as pd
# import random
# import datetime
# import numpy
# N = 100
# s_date = datetime.date(2019, 1, 1)
# e_date = datetime.date(2019, 2, 1)
# Date = []
# for _ in range(N):
#     random_date = s_date + (e_date - s_date) * random.random()
#     Date.append(random_date)
#
# Hourly_Wage =  random.sample(range(10, N), int(N*0.75))
#
# for _ in range(int(N*0.25)):
#     Hourly_Wage.insert(random.randint(0, len(Hourly_Wage)-1), "NAN")
#
# data = {
#     "ID": random.sample(range(1000, 10000), N),
#     "Date":Date,
#     "Time":numpy.random.randint(1, 8, N),
#     "Hourly_Wage":Hourly_Wage
# }
#
# df = pd.DataFrame(data)
# df.to_excel("mid/data.xlsx")








