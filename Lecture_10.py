import pandas as pd
import os.path
import matplotlib.pyplot as plt
from sklearn import linear_model
import numpy as np

PATH = "data/geo_gdp.xls"
if not os.path.exists(PATH):
    data = pd.read_excel("data/gdp.xls", sheet_name="Data", header=3)
    # print(data)
    # print(data["Country Name"])
    geo_gdp_row = data[data["Country Name"]=="Georgia"]
    # print(geo_gdp_row.transpose())
    data_geo = pd.DataFrame(geo_gdp_row.transpose())
    data_geo.to_excel(PATH)
data = pd.read_excel(PATH, skiprows=4)
# print(data)
data.rename(columns={"Indicator Code": "year", "NY.GDP.PCAP.CD": "gdp"}, inplace=True)
# print(data)
data.dropna(subset=["gdp"], inplace=True)
# print(data)
plt.scatter(data.year, data.gdp, marker='x', s=45, color="red")
plt.plot(data.year, data.gdp)
plt.xlabel("Year")
plt.ylabel("GDP")
plt.show()
print(f"Mean = {round(data.gdp.mean(),2)}")
print(f"Median = {data.gdp.median()}")
print(f"STD = {data.gdp.std()}")
print(f"50% percentile = {np.percentile(data.gdp, 50)}")
model = linear_model.LinearRegression()
X = data[["year"]]
y = data.gdp
model.fit(X, y)
print(f"Score = {model.score(X, y)}")
print(f"Coef = {model.coef_}")
print(f"Intercept = {model.intercept_}")
Gdp_predict = [[2022], [2023], [2024], [2025]]
print(f"Prediction = {np.round(model.predict(np.array(Gdp_predict)), 2)}")




