from scrapy.http import TextResponse
import requests
import pandas as pd
import pprint

url = requests.get("https://ss.ge/ka/udzravi-qoneba/l/bina/iyideba?Page=2&RealEstateTypeId=5&RealEstateDealTypeId=4&MunicipalityId=95&CityIdList=95&StatusField.FieldId=34&StatusField.Type=SingleSelect&StatusField.StandardField=Status&PriceType=false&CurrencyId=1")

response = TextResponse(url.url, body=url.text, encoding='utf-8')
# print(response.text)
items = response.css("div.latest_desc div a::attr(href)").getall();
# pprint.pprint(items)
counter = 0
area = []
price = []
rooms = []
for item in items:
    if item != "/ka/home/promopaidservices" and item !="/ka/home/agent":
        url_item = requests.get("https://ss.ge"+item)
        # print(url_item)
        response_item = TextResponse(url_item.url, body=url_item.text, encoding='utf-8')
        info = response_item.css("div.ParamsHdBlk text::text").extract()
        price_item = response_item.css("div.article_right_price::text").extract()
        if len(info)!=0:
            print("https://ss.ge" + item)
            print(info)
            print(price_item[0].strip())
            area.append(info[0][:-2])
            rooms.append(info[1])
            price.append(price_item[0].strip())
            counter += 1
            # if counter==7:
                # break

data_excel  = {
    "area":area,
    "rooms":rooms,
    "price":price
}

df = pd.DataFrame(data_excel)
df.to_excel("data/ss.xls", index=False)
