# dic = {
#         "brand": "Ford",
#         "model": "Mustang",
#         "year": 1964
#       }
# print(dic)
# print(dic["brand"])
# dic = {
#     "price": [30, 40, 41, 42, 46],
#     "count": [3, 5, 5, 5, 23],
#     "name":  ["n1", "n2", "n3", "n4", "n5"]
# }
#
# dic["title"] = ["t1"]
# print(dic)

# f = open("files/demofile.txt", "w")
# f.write("Now the file has more content!")
# f.close()
import os

# f = open("files/demofile.txt", "r")
# print(f.read(3))
# os.remove("files/demofile.txt")
# print(not os.path.exists("files2"))

# if not os.path.exists("files2"):
#     os.mkdir("files2")
#
# if os.path.exists("files1"):
#     os.rmdir("files1")

#==Task3_2===
import os
if not os.path.exists("myFiles"):
    os.mkdir("myFiles")
# f = open("myFiles/data1.txt", "w")
with open("myFiles/data1.txt", "a") as f:
    data =  [*range(0, 11)]
    # data = range(0, 11)
    print(data)
    s = ','.join(str(item) for item in data)
    f.write(s)
# f.close()


list = [1, 4, 5]
print(list)
s = ','.join(str(x) for x in list)
print(s)

for i in range(0, 201):
    print(round(i*0.01, 2))
