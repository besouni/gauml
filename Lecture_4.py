import numpy as np
from numpy import random

# n1 = random.randint(7,10,size=(10))
# print(n1)

n2 = random.choice([3, 5, 7, 9], p=[0.1, 0.3, 0.5, 0.1], size=(10))
print(n2)


# n0 = np.array(9)
# print(n0)

# n1 = np.array([ [ [2, 2, 4], [4, 5, 7]], [[6, 8, 3], [1, 2, 0]]])
# print(n1.shape)
# n2 = n1.reshape(-1)
# print(n2)


# n1 = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
# print(n1)
# print(n1.shape)
# n2 = n1.reshape(3, 4)
# print(n2)
# n3 = n1.reshape(2, 2, 3)
# print(n3)

# arr = np.array([[1, 2, 3], [4, 5, 6]])
# print(arr)
# arr1 =  arr.reshape(-1)
# print(arr1)



# n2 = n1.view()
# print(n1)
# print(n2)
# n1[1] = 9
# print("===========")
# print(n1)
# print(n2)
# print(n1[3:9])
# print(n1[3:9:2])
# print(n1)
# print(n1[2:5])
# print(len(n1))
# print(n1[-1])
# print(n1[-9])
# print(n1[-7:-4])

n2 = np.array([ [2, 3, 4], [4, 7, 5], [6, 6, 3] ])
# print(n2[1,1])

n3 = np.array([ [ [3, 9], [4, 8] ], [ [4, 7], [5, 3] ], [ [6, 6], [3, 1] ] ])
# print(n3)
# print(n3[1, 1, 0])




# l = [ [2, 3, 4], [4, 7, 5], 3]
# print(l)

# arr = np.array([3, 4, 5, 8])
# l = [3, 4, 5, 3]
# print(l)
# print(arr)
# arr1 = np.array(l)
# print(np.__version__)
