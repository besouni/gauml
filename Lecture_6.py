import pandas as pd

# mydataset = {
#   'cars': ["BMW", "Volvo", "Ford"],
#   'passings': [3, 7, 2],
#   'price': [4800, 8099, 2300]
# }
# print(mydataset)
#
# df  = pd.DataFrame(mydataset)
# print(df)
# print(df.cars)
# print(df['cars'])
# print("==============")
# print(df.loc[[1, 2]])
# print(df.head(2))
#
# my_csv =  pd.read_csv("panda/data.csv")
# # print(my_csv)
# print(my_csv.head(10))
#
# print("=============")
# my_excel = pd.read_excel("panda/data.xlsx")
# print(my_excel.head(10))
# print(my_excel.tail(10))
# print("==============")
# print(my_excel.info())
#

# df.to_excel();

# a = mydataset["price"]
# s = pd.Series(a, index = ["x", "y", "z"])
# print(s)
#
# s = pd.Series(mydataset)
# print(s)

data = pd.read_csv("panda/dirtydata.csv")
print(data)
print(data.info())
# data1 = data.drop("Calories", axis="columns")
# print(data1)
# data1 = data.dropna()
# print("===============")
# print(data1)
# data2 =  data.fillna(130)
# print("===========")
# print(data2)
print("===========")
Calories = data.Calories
# print(Calories)
mean = data["Calories"].dropna().mean()
# print(mean)
data['Calories'].fillna(mean, inplace=True)
print(data)