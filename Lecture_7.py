#===Quiz1_1====
# import  random
#
# lec = {}

# l = random.sample(range(1, 100),10)
# print(l)
#
# for x in l:
#     value = x // 10 + x % 10
#     lec[x] = value
# print(lec)

# for i in range(0, 10):
#     r = random.randint(1, 100)
#     value = r//10 + r%10
#     lec[r]  = value
# N = int(input())
# N = 3847
# s = 0
# while N!=0:
#     s += N%10
#     N = N//10
# print(s)
# N = 3847
# N = str(N)
# N1 = list((int(x) for x in N))
# print(N1)
# s = sum(N1)
# print(s)
#
# while len(lec)!=17:
#     r = random.randint(1, 100)
#     value = r // 10 + r % 10
#     lec[r] = value
#
# print(lec)
# print(len(lec))
# print(f"Max - {max(lec.values())}")
# print(f"Max - {max(lec)}")

# lec = {
#     2:2,
#     2:4
# }
#
# print(lec)
#===Quiz1_2====
# import random
import statistics
#
# l = []
# for i in range(1, 11):
#     r = random.randint(5, 15)
#     l.append(r)
# print(l)
# print(l.count(9))
# max(l, key=lambda x: x**2)
# print(f"len - {len(l)}")
# print(f"Mode  - {statistics.multimode(l)}")
#
# s = set(l)
# print(s)
# lec = {}
# for i in l:
#     lec[i] = l.count(i)
# print(lec)


#===Quiz1_3====
import random
alphabet = 'აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰ'
words = []

for _ in range(20):
    w = [alphabet[random.randint(0, len(alphabet)-1)] for i in range(random.randint(5, 15))]
    words.append(''.join(w))

print(words)
d = {}
for w in words:
    vowels = 0
    for c in w:
        if c in 'აეიოუ':
            vowels+=1
    d[w] = vowels

print(d)
m = max(d.values())
# print(m)

for k, v in d.items():
    if v == m:
        print(k)



