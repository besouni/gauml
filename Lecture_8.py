import mysql.connector
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="blog2021"
)
mycursor = mydb.cursor()
# sql = "INSERT INTO users (email, password, firstname) VALUES (%s, %s, %s)"
# val = [("user1@gmail.com", "user123", "user1"), ("user1@gmail.com", "user123", "user1")]
# mycursor.executemany(sql, val)
# mydb.commit()
# print(mycursor.rowcount, "record inserted.")
# print("1 record inserted, ID:", mycursor.lastrowid)


# sql = "SELECT id, email, password FROM users"
# mycursor.execute(sql)
# myresult = mycursor.fetchall()
# print(myresult)
# for item in myresult:
#   print(list(item))

# print("{0} first ".format(4))

# sql = "DELETE FROM users WHERE id=%s"
# val = (5,)
# mycursor.execute(sql, val)
# mydb.commit()
# print(mycursor.rowcount, "record(s) deleted")

# sql = "UPDATE users SET email = %s, password = %s WHERE id = %s"
# val = ("user2@gmail.com", "user2123", 8)
# mycursor.execute(sql, val)
# mydb.commit()


import sshtunnel
import time

sshtunnel.SSH_TIMEOUT = 5.0
sshtunnel.TUNNEL_TIMEOUT = 5.0



with sshtunnel.SSHTunnelForwarder(
    'https://gau.edu.ge/',
    ssh_username="root",
    ssh_password="jghgyuu",
    remote_bind_address=('127.0.0.1', 3306)
) as tunnel:
    mydb = mysql.connector.connect(
        user='',
        password='',
        host='127.0.0.1',
        port=tunnel.local_bind_port,
        database='',
    )