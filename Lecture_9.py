import numpy as np
import  matplotlib.pyplot as mplt
from scipy import stats
age = [18, 20, 20, 27, 28, 30, 22, 28, 29, 24]
# salary = [300,400,400,350,5000,25000,400,1200,1400,800]
salary = [300,400,400,850,1500,1900,700,1200,1400,800]
# mean = round(np.mean(salary), 0)
# print(mean)
# std = round(np.std(salary), 0)
# print(std)
# percentile = np.percentile(salary, 83)
# print(percentile)
slope, intercept, r, p, std_err = stats.linregress(age, salary)
print(slope)
print(intercept)
print(r)
print(f"for 25 age = {slope*25+intercept}")
print(f"for 34 age = {slope*34+intercept}")
mplt.scatter(age, salary)
mplt.show()
